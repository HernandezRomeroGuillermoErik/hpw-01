ObjectA={
  "clave": "abc123",
  "nom_completo": "Hernandez Romero Guillermo",
  "departamento":{"clave":"1016","nombre":"Sistemas"},
  "grupo":[{
      "clave":"abc",
      "nombre":"historia",
      "aula":"a",
      "horario":"4:00-5:00",
           "alumno":[
                      {"num_control":"101608","nom_completo":"Juan Perez Garcia"},
                      {"num_control":"101609","nom_completo":"Rosa Romero Ochoa"},
                    ],
          
          },
          
    {
      "clave":"xyz",
      "nombre":"matematicas",
      "aula":"a",
      "horario":"5:00-6:00",
           "alumno":[
                      {"num_control":"101610","nom_completo":"Juan Roman Riquelme"},
                      {"num_control":"101611","nom_completo":"Roberto Garcia Orozco"},
                      {"num_control":"101612","nom_completo":"Peralta Juarez Miguel"}
                    ],
          
    }
          ]};
          
/*Numero  Total de Alumnos del Profesor */          
function num_alumnos(profe){
    var con=0;
    for( var i=0; i<profe["grupo"].length;i++){
        for(var j=0;j<profe["grupo"][i]["alumno"].length;j++){
            con++;
        }
       
    }
 return con;
}

console.log(num_alumnos(ObjectA));

/*Numero de Alumas especificando el grupo*/

function num_alumnos2(profe,clave){
    var con=0;
    for(var i=0; i<profe["grupo"].length;i++){
        if(clave==profe["grupo"][i]["clave"]){
            con= profe["grupo"][i]["alumno"].length;
        }
        
    }
    return con;
}

console.log(num_alumnos2(ObjectA,"abc"))
